from django.test import TestCase
from django.urls import reverse
from .models import Post
# Create your tests here.

class PostModelTest(TestCase):
    def setUp(self):
        Post.objects.create(text="1st test works")
    
    def test_text_context(self):
        post = post.objects.get(id=1)
        expected_test= post.text
        self.assertEqual(expected_test, '2nd test works')
        
class Home_page_test(TestCase):
    def setUp(self):
        Post.objects.create(text='this is a home page test')

    def view_url_redirect_test(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_byName(self):
        resp = self.client.get(reverse('Home'))
        self.assertEqual(resp.status_code, 200)
    
    def correct_template_test(self):
        resp = self.client.get(reverse('home'))
        self.assertEqual(resp, 'home.html')
